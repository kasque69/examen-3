package com.mycompany.listadinamica;
public class listaDinamica<T> {

    private nodo<T> prime;
    private nodo<T> ulti;
    private int tam;

    public listaDinamica() {
        this.prime = null;
        this.ulti= null;
        this.tam = 0;
    }

    public boolean vacio() {
        return tam == 0;
    }

    public int tamList() {
        return tam;
    }


    private nodo<T> obteNode(int index) {
        if (vacio() || (index < 0 || index >= tamList())) {
            return null;
        } else if (index == 0) {
            return prime;
        } else if (index == tamList() - 1) {
            return ulti;
        } else {
            nodo<T> search = prime;
            int count = 0;
            while (count < index) {
                count++;
                search = search.getNext();
            }
            return search;
        }
    }

    public T get(int index) {
        if (vacio() || (index < 0 || index >= tamList())) {
            return null;
        } else if (index == 0) {
            return prime.getElement();
        } else if (index == tamList() - 1) {
            return ulti.getElement();
        } else {
            nodo<T> search = obteNode(index);
            return search.getElement();
        }
    }

    public T obteprime() {
        if (vacio()) {
            return null;
        } else {
            return prime.getElement();
        }
    }

    public T obteulti() {
        if (vacio()) {
            return null;
        } else {
            return ulti.getElement();
        }
    }

    public T anaprime(T element) {
        nodo<T> newelement;
        if (vacio()) {
            newelement = new nodo<>(element, null);
            prime = newelement;
            ulti = newelement;
        } else {
            newelement = new nodo<T>(element,prime);
            prime = newelement;
        }
        tam++;
        return prime.getElement();
    }

    public T anaulti(T element) {
        nodo<T> newelement;
        if (vacio()) {
            return anaprime(element);
        } else {
            newelement = new nodo<T>(element,null);
            ulti.setNext(newelement);
            ulti = newelement;
        }
        size++;
        return ulti.getElement();
    }

    public T anadir(T element, int index) {
        if (index == 0) {
            return anaprime(element);
        } else if(index == tamList()){
            return anaulti(element);
        }else if((index<0 || index >= tamList())){
            return null;
        }else{
            nodo<T> nodo_prev = obteNode(index - 1);
            nodo<T> nodo_current = obteNode(index);
            nodo<T> newelement = new nodo<>(element,nodo_current);
            nodo_prev.setNext(newelement);
            tam++;
            return obteNode(index).Element();
        }
    }
    
    public String listconte(){
        String str = "";
        if(vacio()){
            str = "LISTA VACIA";
        }else{
           nodo<T> out = prime;
           while(out != null){
               str += out.getElement()+" - ";
               out = out.getNext();
           }
        }
        return str;
    }

}
