/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.listadinamica;

/**
 *
 * @author Blade-Liger
 */
public class nodo<T> {
    private T element;
    private nodo<T> next;//apunta al siguiente nodo
    
    public nodo(T element, nodo<T> next){
        this.element = element;
        this.next = next;
    }

    public T getElement() {//retorna el dato guardado
        return element;
    }

    public void setElement(T element) {//modifica el dato guardado
        this.element = element;
    }

    public nodo<T> getNext() {//siguiente
        return next;
    }

    public void setNext(nodo<T> next) {//modifica a quien apuntamos
        this.next = next;
    }
}
